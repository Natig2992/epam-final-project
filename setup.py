#!/usr/bin/env python3
from setuptools import setup, find_packages
import backend
import frontend



def parse_requirements_file(requirements_path):
    with open(requirements_path) as req_file:
        return req_file.read().strip().split('\n')


setup(
    name="epam-diploma",
    version=frontend.__version__,
    packages=['frontend'],
    entry_points={
        'console_scripts': [
            'epam-diploma=frontend.app:main'
        ],
    },
    include_package_data=True,
    install_requires=parse_requirements_file('frontend/requirements.txt'),
    data_files=[('css', ['frontend/static/css/main.css']),
                ('templates', ['frontend/templates/stress.html', 'frontend/templates/header.html', 'frontend/templates/index.html', 'frontend/templates/update.html'])], 
    )
