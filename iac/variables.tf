variable "AWS_DEFAULT_REGION" {
  type = string
  default = "eu-central-1"
}


variable "my_cidr_vpc" {
  default = "10.0.0.0/20"
}

variable "my_private_subnets" {
  description = "My two private subnets"
  type        = list(string)
  default     = ["10.0.8.0/24", "10.0.9.0/24"]

}



variable "my_public_subnets" {
  description = "My two public subnets"
  type        = list(string)
  default     = ["10.0.10.0/24", "10.0.11.0/24"]
}


variable "environment" {
  default = "dev"
}


variable "app" {
 description = "Frontend and Backend apps"
 type = list
 default = ["frontend", "backend"]
}


variable "ssh_key" {
  default = "aws-epam-1"
}


variable "db_name" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_password" {
  type = string
  
}

variable "tags" {
  type = map
  default = {
    Owner   = "Nagiev Natig"
    Contanct = "nagiev2992@gmail.com"
    Project = "Diploma-epam"
  }
  description = "Default tags"
}
