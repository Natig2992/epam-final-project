
#------------Data----------------#

data "aws_availability_zones" "available" {}


#------------Resources-----------#
resource "aws_vpc" "aws_vpc_epam" {
  cidr_block                       = var.my_cidr_vpc
  enable_dns_hostnames             = true
  enable_dns_support               = true
  instance_tenancy                 = "default"
  assign_generated_ipv6_cidr_block = false
  tags                             = { Name = "VPC-for my epam diploma project" }
}

resource "aws_subnet" "aws_subnet_public_epam" {
  count                   = 2
  vpc_id                  = aws_vpc.aws_vpc_epam.id
  cidr_block              = element(var.my_public_subnets, count.index)
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name        = "Public subnet-${count.index +1} in ${var.environment}"
    Environment = "${var.environment}"
  }
}

resource "aws_subnet" "aws_subnet_private_epam" {
  count                   = 2
  cidr_block              = element(var.my_private_subnets, count.index)
  vpc_id                  = aws_vpc.aws_vpc_epam.id
  map_public_ip_on_launch = "false"
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  tags = {
    Name                              = "Private subnet-${count.index + 1}-in-${var.environment}"
    Environment                       = "${var.environment}"
    "kubernetes.io/cluster/eks"       = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }
}

resource "aws_internet_gateway" "my_epam_gw" {
  vpc_id = aws_vpc.aws_vpc_epam.id
  tags = {
    Name = "My_igw-for-vpc-EPAM"
  }
}

resource "aws_eip" "nat_epam" {
  count = 2

}


resource "aws_nat_gateway" "nat_gw_epam" {
  count         = 2
  allocation_id = aws_eip.nat_epam[count.index].id
  subnet_id     = aws_subnet.aws_subnet_public_epam[count.index].id
  tags = {
    Name        = "Nat-gateway-epam"
    Environment = "${var.environment}"
  }
}

resource "aws_route_table" "my_epam_public_rt" {
  vpc_id = aws_vpc.aws_vpc_epam.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_epam_gw.id
  }

  tags = {
    Name = "Route table public"
  }
}

resource "aws_route_table" "my_epam_private_rt" {
  count  = 2
  vpc_id = aws_vpc.aws_vpc_epam.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gw_epam[count.index].id
  }
  tags = {
    Name = "Route table private"
  }


}

resource "aws_route_table_association" "my_epam_rt_asс_public" {
  count          = 2
  subnet_id      = aws_subnet.aws_subnet_public_epam[count.index].id
  route_table_id = aws_route_table.my_epam_public_rt.id

}

resource "aws_route_table_association" "my_epam_rt_asc_private" {
  count          = 2
  subnet_id      = aws_subnet.aws_subnet_private_epam[count.index].id
  route_table_id = aws_route_table.my_epam_private_rt[count.index].id

}



resource "aws_db_subnet_group" "epam_db_diploma" {
  name       = "epam_db_subnet_group"
  subnet_ids = [aws_subnet.aws_subnet_private_epam[0].id, aws_subnet.aws_subnet_private_epam[1].id]
}




