output "db_prod" {
  value       = aws_db_instance.psql_prod.address
  description = "DB endpoint"
}

output "db_dev" {
  value       = aws_db_instance.psql_dev.address
  description = "DB endpoint"
}
