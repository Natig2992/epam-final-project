#----------------------RDS main config-------------#

resource "aws_security_group" "sg_epam_rds" {
  name        = "SG_for_RDS"
  description = "Allow Postgres inbound traffic"
  vpc_id      = var.vpc_epam
  ingress {
    description = "SG for RDS"
    from_port   = "5432"
    to_port     = "5432"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(var.tags, { Name = "RDS" })
}



resource "aws_db_instance" "psql_prod" {
  identifier                      = "psql-prod"
  engine                          = "postgres"
  engine_version                  = "13"
  instance_class                  = "db.t3.micro"
  db_subnet_group_name            = var.db_subnet_group_name
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  db_name                         = var.db_name
  username                        = var.db_user
  password                        = var.db_password
  publicly_accessible             = false
  allocated_storage               = 20
  max_allocated_storage           = 0
  storage_type                    = "gp2"
  vpc_security_group_ids          = [aws_security_group.sg_epam_rds.id]
  skip_final_snapshot             = true
  storage_encrypted               = true
  backup_retention_period         = 7
  tags                            = merge(var.tags, { Name = "RDS postgresql" })
}

resource "aws_db_instance" "psql_dev" {
  identifier                      = "psql-dev"
  engine                          = "postgres"
  engine_version                  = "13"
  instance_class                  = "db.t3.micro"
  db_subnet_group_name            = var.db_subnet_group_name
  enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]
  db_name                         = var.db_name
  username                        = var.db_user
  password                        = var.db_password
  publicly_accessible             = false
  allocated_storage               = 20
  max_allocated_storage           = 0
  storage_type                    = "gp2"
  vpc_security_group_ids          = [aws_security_group.sg_epam_rds.id]
  skip_final_snapshot             = true
  storage_encrypted               = true
  backup_retention_period         = 7
  tags                            = merge(var.tags, { Name = "RDS postgresql" })
}
