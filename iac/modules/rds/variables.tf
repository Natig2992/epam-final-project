variable "db_name" {
  description = "Database name for my Postgre RDS"
}

variable "db_user" {
  description = "Username for my Postgre RDS database"
}

variable "db_password" {
  description = "Password for my Postgre RDS database"
}

variable "tags" {
  description = "Default tags for RDS"
}

variable "db_subnet_group_name" {
  description = "Subne group name for RDS"

}

variable "vpc_epam" {

  description = "My VPC EPAM"
}
