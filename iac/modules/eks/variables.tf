variable "tags" {
    description = "Tags for Project with EKS"
}

variable "vpc" {
    description = "VPC for Project with EKS"
}

variable "subnets_all" {
    description = "All subnets for Project with EKS"
}

variable "subnets_private" {
    description = "Private subnets for Project with EKS"
}