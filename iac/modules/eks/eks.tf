# For EKS
## Security Group
resource "aws_security_group" "eks-cluster-sg" {
  name   = "eks-cluster-sg"
  vpc_id = var.vpc
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = merge(var.tags, { Name = "EKS" })
}

# Create EKS
## EKS Cluster
resource "aws_iam_role" "eks-cluster" {
  name               = "eks-cluster-epam"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks-cluster-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-cluster.name
}

resource "aws_iam_role_policy_attachment" "eks-vpc-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSVPCResourceController"
  role       = aws_iam_role.eks-cluster.name
}

resource "aws_eks_cluster" "eks" {
  name                      = "eks-epam"
  role_arn                  = aws_iam_role.eks-cluster.arn
  version                   = "1.21"
  enabled_cluster_log_types = ["api", "audit"]
  # depends_on = [aws_cloudwatch_log_group.eks-logs]
  vpc_config {
    endpoint_private_access = false
    endpoint_public_access  = true
    security_group_ids      = [aws_security_group.eks-cluster-sg.id]
    subnet_ids              = var.subnets_all
  }

  kubernetes_network_config {
    service_ipv4_cidr = "192.168.88.0/24"
  }
  tags = merge(var.tags, { Name = "EKS Cluster" })
}

# resource "aws_cloudwatch_log_group" "eks-logs" {
#   name              = "/aws/eks/eks/cluster"
#   retention_in_days = 7
# }


## EKS Node Group
resource "aws_iam_role" "nodes-general" {
  name               = "eks-node-group-general_1"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "eks-worker-node-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.nodes-general.name
}

resource "aws_iam_role_policy_attachment" "eks-cni-node-policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.nodes-general.name
}

resource "aws_iam_role_policy_attachment" "ec2-container-registry-read-only" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.nodes-general.name
}

resource "aws_iam_role_policy_attachment" "cloudwatch-logs-full-access" {
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchLogsFullAccess"
  role       = aws_iam_role.nodes-general.name
}
resource "aws_iam_role_policy_attachment" "AmazonEKSClusterAutoscalerPolicy" {
  policy_arn = "arn:aws:iam::783619924521:policy/AmazonEKSClusterAutoscalerPolicy"
  role       = aws_iam_role.nodes-general.name
}


resource "aws_eks_node_group" "nodes-general" {
  cluster_name    = aws_eks_cluster.eks.name
  node_group_name = "nodes-general_1"
  node_role_arn   = aws_iam_role.nodes-general.arn
  subnet_ids      = var.subnets_private
  scaling_config {
    desired_size = 3
    max_size     = 5
    min_size     = 2
  }
  ami_type             = "AL2_x86_64"
  capacity_type        = "ON_DEMAND"
  disk_size            = 20
  force_update_version = false
  instance_types       = ["t3.medium"]
  labels = {
    role = "nodes-general_1"
  }
  version = "1.21"
  depends_on = [
    aws_iam_role_policy_attachment.eks-worker-node-policy,
    aws_iam_role_policy_attachment.eks-cni-node-policy,
    aws_iam_role_policy_attachment.ec2-container-registry-read-only,
    aws_iam_role_policy_attachment.cloudwatch-logs-full-access,
    aws_iam_role_policy_attachment.AmazonEKSClusterAutoscalerPolicy
  ]
  tags = merge(var.tags, { 
    Name = "nodes-general_1" 
    "k8s.io/cluster-autoscaler/eks-epam" = "owned"
    "k8s.io/cluster-autoscaler/enabled" = "true"
  })
}

#---------------For Tagging eks instance in node-group-----------#

/*
resource "aws_autoscaling_group_tag" "nodes_group" {
  for_each = toset(
    [for asg in flatten(
      [for resources in aws_eks_node_group.nodes-general.resources : resources.autoscaling_groups]
    ) : asg.name]
  )

  autoscaling_group_name = each.value

  tag {
    key   = "Name"
    value = "eks_node_group"
    propagate_at_launch = true
  }
  depends_on = [aws_eks_node_group.nodes-general]

}
*/
