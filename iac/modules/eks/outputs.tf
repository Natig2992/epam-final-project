output "eks_cluster_name" {
  value       = aws_eks_cluster.eks.name
  description = "EKS cluster name"
}

output "eks_cluster_ca" {
  value       = aws_eks_cluster.eks.certificate_authority
  description = "EKS cluster name"
}

