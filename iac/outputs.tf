
output "db-prod" {
  value = module.rds.db_prod
  description = "DB endpoint"
}

output "db-dev" {
  value = module.rds.db_dev
  description = "DB endpoint"
}

output "app-repo-uri" {
 value = aws_ecr_repository.epam_diploma_rep[*].repository_url 
 description = "repository url" 
}

output "eks_cluster_name" {
  value       = module.eks.eks_cluster_name
  description = "EKS cluster name"
}

/*
 output "eks_cluster_certificate_authority" {
  value = module.eks.eks_cluster_ca
}

*/
