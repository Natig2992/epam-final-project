#-------------ECR----------------#

resource "aws_ecr_repository" "epam_diploma_rep" {
  count = 2
  name                 = "beatles_epam_${element(var.app, count.index)}"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

#-------------RDS----------------#

module "rds" {
  source               = "./modules/rds"
  vpc_epam             = aws_vpc.aws_vpc_epam.id
  db_subnet_group_name = aws_db_subnet_group.epam_db_diploma.name
  db_name              = var.db_name
  db_user              = var.db_user
  db_password          = var.db_password
  tags                 = var.tags

}

#---------------EKS----------------#

module "eks" {
  source          = "./modules/eks"
  subnets_all     = [aws_subnet.aws_subnet_public_epam[0].id, aws_subnet.aws_subnet_public_epam[1].id, aws_subnet.aws_subnet_private_epam[0].id, aws_subnet.aws_subnet_private_epam[1].id]
  subnets_private = [aws_subnet.aws_subnet_private_epam[0].id, aws_subnet.aws_subnet_private_epam[1].id]
  vpc             = aws_vpc.aws_vpc_epam.id
  tags            = var.tags
}