terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.5.0"
    }
  }
  required_version = ">= 1.1.7"
}

provider "aws" {
  region     = "${var.AWS_DEFAULT_REGION}"
  
  default_tags {
    tags = {
      Owner = "Nagiev Natig"
      Contact = "nagiev2992@gmail.com"
      Project = "Diploma-beatles-collections"
    }
  }
}

terraform {

  backend "http" {
  
  }
}
